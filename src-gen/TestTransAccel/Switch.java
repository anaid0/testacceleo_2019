package TestTransAccel;

public class Switch  
{
	// Atrybuty:
	public DecisionNode base_DecisionNode;
	public String expr;

	// Getters & Setters:
	public DecisionNode getBase_DecisionNode() { return base_DecisionNode; }
	public void setBase_DecisionNode(DecisionNode w) { base_DecisionNode = w; }
	public String getExpr() { return expr; }
	public void setExpr(String w) { expr = w; }
	// Operacje:
};
